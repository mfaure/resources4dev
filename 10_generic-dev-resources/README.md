# Generic resources for web development

## Security
- [ ] [Overview of web security (Stanford course)](https://web.stanford.edu/class/cs253/) - Most common web attacks and their countermeasures

## Best practices

- [ ] [Twelve-Factor-App ---> 12factor.net](https://12factor.net/fr/) - Methodology for building software-as-a-service apps
- [ ] [Refactoring.guru](https://refactoring.guru/) - Refactoring, design patterns, SOLID principles, and other smart programming topics)

## Regular expression (Regex)

- [Regex101.com](https://regex101.com/) - Online regex tester, debugger with highlighting for PHP, PCRE, Python, Golang and JavaScript.
- [Regexr.com](https://regexr.com) - Online regex tester, PHP / PCRE & JS Support, contextual help, cheat sheet, reference, and searchable community patterns.
