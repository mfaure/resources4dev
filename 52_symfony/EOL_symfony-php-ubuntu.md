# Symfony-PHP-EOL

Map of respective end-of-life for releases of Symfony, PHP, Debian, Ubuntu.

Bsed on the document  [mfaure/symfony-php-eol](https://gitlab.adullact.net/mfaure/symfony-php-eol)


## End of life (EOL)

### PHP

* [PHP Supported versions](https://www.php.net/supported-versions.php)
* [Migrating from one version of PHP to another version](https://www.php.net/manual/fr/appendices.php)

| PHP version  | Released     | Active Support Until | Security Support Until |
|--------------|-------------:|---------------------:|-----------------------:|
| 7.2          | 30 Nov 2017  | 30 Nov 2019          | 30 Nov 2020            |
| **7.3**      | 6 Dec 2018   | 6 Dec 2020           | **6 Dec 2021**         |
| **7.4**      | 28 Nov 2019  | 28 Nov 2021          | **28 Nov 2022**        |
| 8.0  `(2)`   | Nov 2020     |                      |                        |

`(2)` not release yet

### Symfony

* [Symfony Releases](https://symfony.com/releases)
* [Symfony pre-requisites](https://symfony.com/doc/master/setup.html#technical-requirements)
* [Backward Compatibility Promise](https://symfony.com/doc/master/contributing/code/bc.html)
* [Release Process](https://symfony.com/doc/master/contributing/community/releases.html)
    * **Patch** version
        * every month
        * only contains bug fixes
    * **Minor** version
        * every six months (one in May and one in November)
        * only  contains bug fixes and new features, but it doesn't include any breaking change)
    * **Major** version
        * every two years
        * contains breaking changes
        * change the necessary version of PHP

| Symfony      | at least PHP | Released       | End of support | End of bug fixes | End of security fixes |
|--------------|-------------:|---------------:|---------------:|-----------------:|----------------------:|
| **4.4 LTS**  | 7.1.3        | November 2019  |                | November 2022    | November 2023         |
| **5.0**      | 7.2.5        | November 2019  | **July 2020**  |                  |                       |
| 5.1 `(2)`    | 7.2.5        | May 2020       | January 2021   |                  |                       |
| 5.2 `(2)`    | 7.2.5        | November 2020  | July 2021      |                  |                       |
| 5.3 `(2)`    | 7.2.5        | May 2021       | January 2022   |                  |                       |
| 5.4 LTS `(2)`| PHP 7.2.5    | November 2021  | November 2024  |             ?    |     ?                 |

`(2)` not release yet


> This deprecation policy also requires a custom development process for major versions (4.0, 5.0, 6.0, etc.) In those cases, Symfony develops at the same time two versions: the new major one (e.g. 5.0) and the latest version of the previous branch (e.g. 4.4).
>
> Both versions have the same new features, but they differ in the deprecated features. The oldest version (4.4 in this example) contains all the deprecated features whereas the new version (5.0 in this example) removes all of them.


### Ubuntu

* [Ubuntu lifecycle](https://ubuntu.com/about/release-cycle)
* [Ubuntu list of releases (past + future)](https://wiki.ubuntu.com/Releases)
* [PHP 7.4 in 20.04](https://discourse.ubuntu.com/t/php-7-4-in-focal/15000)
* [Upcoming PHP 7.4 transition](https://lists.ubuntu.com/archives/ubuntu-devel/2020-February/040901.html)

### Debian

* [Debian Releases official](https://www.debian.org/releases/)
* [Wiki Debian releases Production](https://wiki.debian.org/DebianReleases#Production_Releases)
* [PHP 7.3 in Debian 10 Buster](https://wiki.debian.org/DebianBuster)

## Result of combinations

| Distro family | Distro version    | Until  | Shipped with PHP  | Usable versions of Symfony |
|---------------|------------------:|-------:|------------------:|---------------------------:|
| Ubuntu        | 18.04             | 2023   | 7.2               | 4.4 + 5.x                  |
| Ubuntu        | 20.04             | 2025   | 7.4               | 4.4 + 5.x                  |
| Debian        | *Stretch*  9      | ~2020  | 7.0               | NA                         |
| Debian        | *Buster*  10      | ~2022  | 7.3               | 4.4 + 5.x                  |
